package com.summatech.tburley.mytictactoe;

import java.util.Scanner;

/**
 * Created by Toni on 9/15/2015.
 */
public class Game {

    public static void main(String[] args) {

       playGame();

    }

    public static void askForNewGame(){

        char didTheySayYes = 'Y';

        Scanner inputDevice = new Scanner(System.in);
        System.out.println();
        System.out.println("Would You Like to Play Again?" + "\n" + "Enter Y or N");
        StringBuilder bufferInput = new StringBuilder(inputDevice.nextLine());
        char doYouWantToStartAnewGame = bufferInput.charAt(0);

        if (didTheySayYes == doYouWantToStartAnewGame)
        {
            playGame();
        }
    }public static Boolean checkForWin(Board board, int player){
        String symbol = player == 1 ? "XXXPlayer 1" : "OOOPlayer 2";

        Boolean thereIsAWin = false;
//        TODO: find a way to refactor this and make it more pragmatic

        String win1 =""+ board.cell1 + board.cell2 + board.cell3; // Q: Why are the cells returning int?
        String win2 =""+ board.cell4 + board.cell5 + board.cell6;
        String win3 =""+ board.cell7 + board.cell8 + board.cell9;
        String win4 =""+ board.cell1 + board.cell5 + board.cell9;
        String win5 =""+ board.cell7 + board.cell5 + board.cell3;
        String win6 =""+ board.cell1 + board.cell4 + board.cell7;
        String win7 =""+ board.cell2 + board.cell5 + board.cell8;
        String win8 =""+ board.cell3 + board.cell6 + board.cell9;

        String[] possibleWins = {win1, win2, win3, win4, win5, win6, win7, win8};

        for (int i = 0; i < possibleWins.length; i++) {
            if (symbol.regionMatches(0, possibleWins[i], 0, 3)){
                thereIsAWin = declareAWin(symbol, thereIsAWin);
            }
        }

        return thereIsAWin;
    }

    public static Boolean declareAWin(String symbol, Boolean winningState){ // Q: How do I resolve this?

        System.out.println();
        System.out.println("Congrats!!" + " " + symbol.substring(3));
        return true;
    }

    public static Board initializeBoard(){
        Board newBoard = new Board();
        newBoard.showBoard();
        return newBoard;
    }

    public static void playGame(){

        Boolean someoneWon = false;

        Board currentBoardPlaying = initializeBoard();

        do {
            currentBoardPlaying.addMove(playerTurn(1), 1);
            if (checkForWin(currentBoardPlaying, 1)) {
                someoneWon = true
                ;
            } else {
                currentBoardPlaying.addMove(playerTurn(2), 2);
                if (checkForWin(currentBoardPlaying, 2)) {
                    someoneWon = true;
                }
            }
        }
        while (!someoneWon);

        askForNewGame();

    }

    private static char playerTurn(int playerNumber){
        char move;
        Scanner inputDevice = new Scanner(System.in);
        System.out.println();
        System.out.println("Player " + playerNumber + ", please select your move >> ");
        StringBuilder bufferInput = new StringBuilder(inputDevice.nextLine());
        move = bufferInput.charAt(0);
        System.out.println("You selected " + move);
        System.out.println();
        return move;
    }

}


